<?php

namespace App\type;

use App\value\QualityStep;

class BqqBooze implements TypeInterface
{
    const NAME = 'BBQ - Afkoop drank';

    /**
     * @var int
     */
    private $quality;

    /**
     * @var int
     */
    private $sellBefore;

    /**
     * @param int $quality
     * @param int $sellBefore
     */
    public function __construct($quality, $sellBefore)
    {
        $this->quality = $quality;
        $this->sellBefore = $sellBefore;
    }

    /**
     * @inheritdoc
     */
    public function getMaxQuality()
    {
        return 80;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @inheritdoc
     */
    public function getSellBefore()
    {
        return $this->sellBefore;
    }

    /**
     * @inheritdoc
     */
    public function handleTick()
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFirstSellBeforeThreshold()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedSecondSellBeforeThreshold()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFinalSellBeforeThreshold()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function downgradeQuality(QualityStep $quality)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function upgradeQuality(QualityStep $quality)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function downgradeSellBefore()
    {
        return;
    }
}
