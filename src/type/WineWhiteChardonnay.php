<?php

namespace App\type;

use App\value\QualityStep;

class WineWhiteChardonnay implements TypeInterface
{
    const NAME = 'Witte Wijn - Chardonnay';

    /**
     * @var int
     */
    private $quality;

    /**
     * @var int
     */
    private $sellBefore;

    /**
     * @param int $quality
     * @param int $sellBefore
     */
    public function __construct($quality, $sellBefore)
    {
        $this->quality = $quality;
        $this->sellBefore = $sellBefore;
    }

    /**
     * @inheritdoc
     */
    public function getMaxQuality()
    {
        return self::MAX_QUALITY;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     */
    public function getQuality()
    {
        return $this->quality > self::MAX_QUALITY ? self::MAX_QUALITY : $this->quality;
    }

    /**
     * @inheritdoc
     */
    public function getSellBefore()
    {
        return $this->sellBefore;
    }


    /**
     * @inheritdoc
     */
    public function handleTick()
    {
        $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP);

        if ($this->hasReachedFirstSellBeforeThreshold()) {
            if ($this->hasReachedSecondSellBeforeThreshold()) {
                $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP * 3);
            } else {
                $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP * 2);
            }
        }

        $this->upgradeQuality($qualityStep);

        $this->downgradeSellBefore();

        if ($this->hasReachedFinalSellBeforeThreshold()) {
            $this->downgradeQuality(new QualityStep($this->getQuality()));
        }
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFirstSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_FIRST;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedSecondSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_SECOND;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFinalSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_FINAL;
    }

    /**
     * @inheritdoc
     */
    public function downgradeQuality(QualityStep $quality)
    {
        $this->quality = $this->quality - $quality->getStep();
    }

    /**
     * @inheritdoc
     */
    public function upgradeQuality(QualityStep $quality)
    {
        if ($this->quality >= self::MAX_QUALITY) {
            return;
        }

        $this->quality = $this->quality + $quality->getStep();
    }

    /**
     * @inheritdoc
     */
    public function downgradeSellBefore()
    {
        $this->sellBefore--;
    }
}
