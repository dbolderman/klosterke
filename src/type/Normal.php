<?php

namespace App\type;

use App\value\QualityStep;

class Normal implements TypeInterface
{
    const NAME = 'Normaal item';

    /**
     * @var int
     */
    private $quality;

    /**
     * @var int
     */
    private $sellBefore;

    /**
     * @param int $quality
     * @param int $sellBefore
     */
    public function __construct($quality, $sellBefore)
    {
        $this->quality = $quality;
        $this->sellBefore = $sellBefore;
    }

    /**
     * @inheritdoc
     */
    public function getMaxQuality()
    {
        return self::MAX_QUALITY;
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @inheritdoc
     */
    public function getSellBefore()
    {
        return $this->sellBefore;
    }

    /**
     * @inheritdoc
     */
    public function handleTick()
    {
        $this->downgradeSellBefore();

        $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP);

        if ($this->hasReachedFinalSellBeforeThreshold()) {
            $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP * 2);
        }

        $this->downgradeQuality($qualityStep);
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFirstSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_FIRST;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedSecondSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_SECOND;
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFinalSellBeforeThreshold()
    {
        return $this->sellBefore < self::SELLBEFORE_THRESHOLD_FINAL;
    }

    /**
     * @inheritdoc
     */
    public function downgradeQuality(QualityStep $quality)
    {
        if ($this->quality <= 0) {
            return;
        }

        $this->quality = $this->quality - $quality->getStep();
    }

    /**
     * @inheritdoc
     */
    public function upgradeQuality(QualityStep $quality)
    {
        return;
    }

    /**
     * @inheritdoc
     */
    public function downgradeSellBefore()
    {
        $this->sellBefore--;
    }
}
