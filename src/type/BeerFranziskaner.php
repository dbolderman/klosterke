<?php

namespace App\type;

use App\value\QualityStep;

class BeerFranziskaner extends Normal implements TypeInterface
{
    const NAME = 'Kloosterbier - Franziskaner';

    /**
     * @param int $quality
     * @param int $sellBefore
     */
    public function __construct($quality, $sellBefore)
    {
        parent::__construct($quality, $sellBefore);
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * @inheritdoc
     */
    public function handleTick()
    {
        $this->downgradeSellBefore();

        $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP * 2);

        if ($this->hasReachedFinalSellBeforeThreshold()) {
            $qualityStep = new QualityStep(QualityStep::DEFAULT_STEP * 4);
        }

        $this->downgradeQuality($qualityStep);
    }

    /**
     * @inheritdoc
     */
    public function hasReachedFinalSellBeforeThreshold()
    {
        return parent::getSellBefore() <= self::SELLBEFORE_THRESHOLD_FINAL;
    }
}
