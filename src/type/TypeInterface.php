<?php

namespace App\type;

use App\value\QualityStep;

interface TypeInterface
{
    const MAX_QUALITY = 50;
    const SELLBEFORE_THRESHOLD_FIRST = 11;
    const SELLBEFORE_THRESHOLD_SECOND = 6;
    const SELLBEFORE_THRESHOLD_FINAL = 0;

    /**
     * @return int
     */
    public function getMaxQuality();

    /**
     * @return string $name
     */
    public function getName();

    /**
     * @return int $quality
     */
    public function getQuality();

    /**
     * @return int $sellBefore
     */
    public function getSellBefore();

    /**
     * @return void
     */
    public function handleTick();

    /**
     * @return bool
     */
    public function hasReachedFirstSellBeforeThreshold();

    /**
     * @return bool
     */
    public function hasReachedSecondSellBeforeThreshold();

    /**
     * @return bool
     */
    public function hasReachedFinalSellBeforeThreshold();

    /**
     * @param QualityStep $quality
     *
     * @return void
     */
    public function downgradeQuality(QualityStep $quality);

    /**
     * @param QualityStep $quality
     *
     * @return void
     */
    public function upgradeQuality(QualityStep $quality);

    /**
     * @return void
     */
    public function downgradeSellBefore();
}
