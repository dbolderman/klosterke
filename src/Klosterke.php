<?php

namespace App;

use App\type\TypeInterface;

class Klosterke
{
    /**
     * @var TypeInterface
     */
    private $item;

    /**
     * @param TypeInterface $item
     */
    public function __construct(TypeInterface $item)
    {
        $this->item = $item;
    }

    /**
     * @return void
     */
    public function tick()
    {
        $this->item->handleTick();
    }
}
