<?php

namespace App\value;

use InvalidArgumentException;

final class QualityStep
{
    const DEFAULT_STEP = 1;

    /**
     * @var int
     */
    private $step;

    /**
     * @param int $multiplier
     *
     * @throws InvalidArgumentException
     */
    public function __construct($multiplier)
    {
        if (!$multiplier || $multiplier === 0) {
            throw new InvalidArgumentException('No multiplier given!');
        }

        $this->step = self::DEFAULT_STEP * $multiplier;
    }

    /**
     * @return int
     */
    public function getStep()
    {
        return (int) $this->step;
    }
}
