<?php

use App\Klosterke;
use App\type\BeerFranziskaner;
use App\type\BqqBooze;
use App\type\Normal;
use App\type\WineRedMerlot;
use App\type\WineWhiteChardonnay;

describe('Klosterke', function () {

    describe('#tick', function () {

        context ('normale Items', function () {

            it ('Check het type en de naam van het item', function() {
                $type = new Normal(10, 10);

                expect($type)->toBeAnInstanceOf(Normal::class);
                expect($type->getName())->toBe('Normaal item');
            });

            it ('update normal items voor de verkoopdatum', function () {
                $type = new Normal(10, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(9);
                expect($type->getSellBefore())->toBe(4);
            });

            it ('update normal items op de verkoopdatum', function () {
                $type = new Normal(10, 0);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(8);
                expect($type->getSellBefore())->toBe(-1);
            });

            it ('update normal items na de verkoopdatum', function () {
                $type = new Normal(10, -5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(8);
                expect($type->getSellBefore())->toBe(-6);
            });

            it ('update normal items with a quality of 0', function () {
                $type = new Normal(0, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(0);
                expect($type->getSellBefore())->toBe(4);
            });

        });


        context('Rode Wijn items', function () {

            it ('Check het type en de naam van het item', function() {
                $type = new WineRedMerlot(10, 10);

                expect($type)->toBeAnInstanceOf(WineRedMerlot::class);
                expect($type->getName())->toBe('Rode Wijn - Merlot');
            });

            it ('update Rode Wijn items voor de verkoopdatum', function () {
                $type = new WineRedMerlot(10, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(11);
                expect($type->getSellBefore())->toBe(4);
            });

            it ('update Rode Wijn items voor de verkoopdatum with maximum quality', function () {
                $type = new WineRedMerlot(50, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(4);
            });

            it ('update Rode Wijn items op de verkoopdatum', function () {
                $type = new WineRedMerlot(10, 0);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(12);
                expect($type->getSellBefore())->toBe(-1);
            });

            it ('update Rode Wijn items op de verkoopdatum, nabij de maximale kwaliteit', function () {
                $type = new WineRedMerlot(49, 0);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(-1);
            });

            it ('update Rode Wijn items op de verkoopdatum met de maximale kwaliteit', function () {
                $type = new WineRedMerlot(50, 0);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(-1);
            });

            it ('update Rode Wijn items na de verkoopdatum', function () {
                $type = new WineRedMerlot(10, -10);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(12);
                expect($type->getSellBefore())->toBe(-11);
            });

             it ('update Rode Wijn items na de verkoopdatum met de maximale kwaliteit', function () {
                 $type = new WineRedMerlot(150, -10);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(50);
                 expect($type->getSellBefore())->toBe(-11);
            });

        });


        context('BBQ items', function () {

            it ('Check het type en de naam van het item', function() {
                $type = new BqqBooze(10, 10);

                expect($type)->toBeAnInstanceOf(BqqBooze::class);
                expect($type->getName())->toBe('BBQ - Afkoop drank');
            });

            it ('update BBQ items voor de verkoopdatum', function () {
                $type = new BqqBooze(10, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(10);
                expect($type->getSellBefore())->toBe(5);
            });

            it ('update BBQ items op de verkoopdatum', function () {
                $type = new BqqBooze(10, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(10);
                expect($type->getSellBefore())->toBe(5);
            });

            it ('update BBQ items na de verkoopdatum', function () {
                $type = new BqqBooze(10, -1);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(10);
                expect($type->getSellBefore())->toBe(-1);
            });

        });


        context('Witte Wijn', function () {
            /*
                "Witte Wijn", net zoals Rode Wijn, lopen op in kwaliteit als de verkoopVoor 
                datum dichterbij komt; Kwaliteit verhoogt met 2 wanneer er 10 of minder dagen 
                te gaan zijn en met 3 wanneer er 5 of minder dagen te gaan zijn. Wanneer de verkoopVoor
                datum is gepasseerd keldert de waarde naar 0
             */

            it ('Check het type en de naam van het item', function() {
                $type = new WineWhiteChardonnay(10, 10);

                expect($type)->toBeAnInstanceOf(WineWhiteChardonnay::class);
                expect($type->getName())->toBe('Witte Wijn - Chardonnay');
            });

            it ('update Witte Wijn items lang voor de verkoopdatum', function () {
                $type = new WineWhiteChardonnay(10, 11);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(11);
                expect($type->getSellBefore())->toBe(10);
            });

            it ('update Witte Wijn items dicht bij de verkoopdatum', function () {
                $type = new WineWhiteChardonnay(10, 10);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(12);
                expect($type->getSellBefore())->toBe(9);
            });

            it ('update Witte Wijn items dicht bij de verkoopdatum op de maximale kwaliteit', function () {
                $type = new WineWhiteChardonnay(50, 10);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(9);
            });

            it ('update Witte Wijn items dicht bij de verkoopdatum', function () {
                $type = new WineWhiteChardonnay(10, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(13); // goes up by 3
                expect($type->getSellBefore())->toBe(4);
            });

            it ('update Witte Wijn items dicht bij de verkoopdatum op de maximale kwaliteit', function () {
                $type = new WineWhiteChardonnay(50, 5);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(4);
            });

            it ('update Witte Wijn items met slechts 1 dag te gaan', function () {
                $type = new WineWhiteChardonnay(10, 1);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(13);
                expect($type->getSellBefore())->toBe(0);
            });

            it ('update Witte Wijn items met slechts 1 dag te gaan op de maximale kwaliteit', function () {
                $type = new WineWhiteChardonnay(50, 1);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(50);
                expect($type->getSellBefore())->toBe(0);
            });

            it ('update Witte Wijn items op de verkoopdatum', function () {
                $type = new WineWhiteChardonnay(10, 0);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(0);
                expect($type->getSellBefore())->toBe(-1);
            });

            it ('update Witte Wijn items na de verkoopdatum', function () {
                $type = new WineWhiteChardonnay(10, -1);
                $item = new Klosterke($type);

                $item->tick();

                expect($type->getQuality())->toBe(0);
                expect($type->getSellBefore())->toBe(-2);
            });

        });


         context ("Kloosterbier items", function () {

             it ('Check het type en de naam van het item', function() {
                 $type = new BeerFranziskaner(10, 10);

                 expect($type)->toBeAnInstanceOf(BeerFranziskaner::class);
                 expect($type->getName())->toBe('Kloosterbier - Franziskaner');
             });

             it ('update Conjured items voor de verkoopdatum', function () {
                 $type = new BeerFranziskaner(10, 10);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(8);
                 expect($type->getSellBefore())->toBe(9);
             });

             it ('update Kloosterbier items op de minimale kwaliteit', function () {
                 $type = new BeerFranziskaner(0, 10);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(0);
                 expect($type->getSellBefore())->toBe(9);
             });

             it ('update Kloosterbier items op de verkoopdatum', function () {
                 $type = new BeerFranziskaner(10, 0);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(6);
                 expect($type->getSellBefore())->toBe(-1);
             });

             it ('update Kloosterbier items op de verkoopdatum op de minimale kwaliteit', function () {
                 $type = new BeerFranziskaner(0, 0);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(0);
                 expect($type->getSellBefore())->toBe(-1);
             });

             it ('update Kloosterbier items na de verkoopdatum', function () {
                 $type = new BeerFranziskaner(10, -10);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(6);
                 expect($type->getSellBefore())->toBe(-11);
             });

             it ('update Kloosterbier items na de verkoopdatum op de minimale kwaliteit', function () {
                 $type = new BeerFranziskaner(0, -10);
                 $item = new Klosterke($type);

                 $item->tick();

                 expect($type->getQuality())->toBe(0);
                 expect($type->getSellBefore())->toBe(-11);
             });

         });

    });

});
